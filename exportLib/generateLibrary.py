import pandas as pd
import json
import os
import shutil

target_cols = {
    "title": {
        "name": "title",
        "type": str
    },
    "author_details": {
        "name": "author",
        "type": str
    },
    "book_uuid": {
        "name": "img",
        "type": str
    },
    "publisher": {
        "name": "publisher",
        "type": str
    },
    "date_published": {
        "name": "year",
        "type": str
    },
    "description": {
        "name": "summary",
        "type": str
    },
    "bookshelf": {
        "name": "category",
        "type": str
    },
    "pages": {
        "name": "pages",
        "type": str
    },
}


def generate_json(df):
    result = []
    df = df.reset_index()
    for i, row in df.iterrows():
        fields = {}
        for col in df.columns:
            fields[col] = "" if str(row[col]) == "nan" else str(row[col])
        fields["show"] = True
        result.append(fields)
    return result


final_list = []
isbn_list = []
for dir_name in os.listdir("."):
    if os.path.isdir(dir_name):
        print("Processing " + dir_name + "...")
        df = pd.read_csv(
            os.path.join(dir_name, "export.csv"),
            usecols=list(target_cols) + ["isbn"],
            dtype=str # Change for <target_cols> type
        )

        # Remove duplicates
        df = df.drop_duplicates(subset=["isbn"])
        df = df[~df["isbn"].isin(isbn_list)]
        isbn_list += df["isbn"].tolist()
        df.drop(columns=["isbn"])
        
        # Remove comma from bookshelf
        df["bookshelf"] = df["bookshelf"].apply(lambda x : x[:-1])
        # Remove "|" from authors
        df["author_details"] = df["author_details"].apply(lambda x : x.replace("|"," & "))

        # Transform dataframe to list of strings
        df.rename(columns={
            key: values["name"] for key, values in target_cols.items()
        }, inplace=True)
        final_list += generate_json(df)
        for jpg in os.listdir(dir_name):
            if jpg[-3:] == "jpg":
                shutil.copy(dir_name+"/"+jpg, "../imgbooks")


with open("../books.js", "w") as f:
    f.write("booksData = ")
    json.dump(final_list, f, indent=4)

print("Finished!")
