# Manual de uso

El objetivo de este manual es explicar paso a paso cómo generar la base de datos de libros para que se pueda visualizar desde la web. Los libros se escanean con una aplicación de móvil. Esta descarga la información de los libros. Y luego con un script se parsea para que la página web pueda leer la información.

## 1. Decargar aplicación

El primer paso es descargarse la aplicación del móvil:

Desde F-Droid (hace falta activar el repositorio de F-Droid Archive, Opciones > Repositorios > F-Droid Archive):

https://test.f-droid.org/en/packages/com.eleybourn.bookcatalogue/index.html

Desde Play Store (mejor si es desde Aurora Store):

https://play.google.com/store/apps/details?id=com.eleybourn.bookcatalogue&hl=es&gl=US

## 2. Escanear libros

Para escanear los libros, abrimos la aplicación y vamos a: Añadir libro > Escanear códifo de barras/ISBN.

Por defecto, la categoría de los libros será "Default". Para cambiarlo hay que ir a: Administración y Preferencias > Gestionar bibliotecas. Y aquí cambiamos el nombre de la categoría, por ejemplo "Novela". Otra opción (más cómoda) es cambiar la categoría una vez que tengamos el archivo final, lo veremos más tarde.

## 3. Generar CSV en el móvil

Una vez escaneados los libros que queremos exportar en este momento, hacemos lo siguiente: Administración y Preferencias > Exportar a fichero CSV > Cancelar. Esto generará un archivo CSV en la carpeta "bookCatalogue". Además, en esta carpeta encontraremos las fotos de las portadas de los libros.

## 4. Pasar libros al ordenador

Para pasar los libros al ordenador, tenemos que copiar la carpeta "bookCatalogue" (esta contiene el CSV y las imagenes) que se ha generado en el móvil y pegarla dentro de la carpeta "exportLib" de este repositorio. Podemos pasar varias carpetas de varios móviles y pegarlas con distintos nombres.

## 5. Generar base de datos para la página web

Una vez pasadas todas las carpetas que queramos, entramos con la terminal a la carpeta "exportLib" y ejecutamos:

`python3 generateLibrary.py`

Esto generará un archivo llamado "book.js" en la carpeta raíz con la información de los libros. Además, copiará las imágenes de las portadas a la carpeta "imgbooks".

Ya tenemos la página web lista. Para visualizarla, abrimos el archivo "index.html" con firefox.

# TODO

- [x] quitar html tags
- [x] responsive para moviles
- [x] descargar librerias (bootstrap, vuejs)
- [x] quitar ".0" de las páginas
- [x] formatear autores y categoría correctamente
- [x] Hacer manual para replicar la página
- [ ] Mejorar buscador con móvil
- [ ] Mejorar tiempo de carga

