const accentsMap = new Map([
  ["A", "Á|À|Ã|Â|Ä"],
  ["a", "á|à|ã|â|ä"],
  ["E", "É|È|Ê|Ë"],
  ["e", "é|è|ê|ë"],
  ["I", "Í|Ì|Î|Ï"],
  ["i", "í|ì|î|ï"],
  ["O", "Ó|Ò|Ô|Õ|Ö"],
  ["o", "ó|ò|ô|õ|ö"],
  ["U", "Ú|Ù|Û|Ü"],
  ["u", "ú|ù|û|ü"],
  ["C", "Ç"],
  ["c", "ç"],
  ["N", "Ñ"],
  ["n", "ñ"]
]);

const reducer = (acc, [key]) => acc.replace(new RegExp(accentsMap.get(key), "g"), key);

const removeAccents = (text) => [...accentsMap].reduce(reducer, text);

var app = new Vue({
	el: '#app',
	data: {
		books: booksData,
		search_text:"",
		nothing:false
	},
	methods: {
		search() {
			something = false;
			this.books.forEach((book) => {
				search_text_lower = removeAccents(this.search_text).toLowerCase()
				book.show = book.title_lower.includes(search_text_lower) || book.author_lower.includes(search_text_lower) ||
									book.summary_lower.includes(search_text_lower) || book.publisher_lower.includes(search_text_lower);
				something |= book.show;
				});
		this.nothing = !something;
		},
		atStart() {
			this.books.forEach((book) => {
				title_lower = removeAccents(book.title).toLowerCase()
				book.title_lower = title_lower;
				author_lower = removeAccents(book.author).toLowerCase()
				book.author_lower = author_lower;
				summary_lower = removeAccents(book.summary).toLowerCase()
				book.summary_lower = summary_lower;
				publisher_lower = removeAccents(book.publisher).toLowerCase()
				book.publisher_lower = publisher_lower;
			});
		}
	}
});

app.atStart()